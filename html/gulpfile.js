const gulp = require("gulp")
const autoprofixer = require("gulp-autoprefixer")

gulp.task("default", function() {
	gulp.src('css/**')
		.pipe(autoprofixer({
			browsers: ['last 10 versions', 'Android >= 4.0'],

			// 是否美化属性值 默认：true 像这样：
			// -webkit-transform: rotate(45deg);
			//        transform: rotate(45deg);
			cascade: true,
			remove: false // 是否去掉不必要的前缀 默认：true 
		}))
		.pipe(gulp.dest("gulp-css"))
})