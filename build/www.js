const express = require('express'); //托管静态资源
const app = express();

const PORT = 9002; //服务端分配端口，需与nginx配置保持一致
const PATH = '/search'; //服务端分配的路径，需与nginx配置保持一致，虚拟路径，文件不在这里
const ROOT = 'dist'; //静态资源根目录

//输出请求，便于调试
app.use(function (req, res, next) {
  console.log(`${new Date()}: ${req.originalUrl}`);
  next();
});

//可访问目录，及其访问路径
app.use(PATH,express.static(ROOT));

//提示部署成功
app.listen(PORT, () => console.log(`Success!! Listening on port: ${PORT}, access path: ${PATH}`))