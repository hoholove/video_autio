
// 构建生产文件
'use strict'
require('../webpack/check-versions')()

process.env.NODE_ENV = 'production' // 设置为生产环境

const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const config = require('../config')
const webpackConfig = require('../webpack/webpack.prod.conf')

const spinner = ora('>>>>>>> 打包生产版本中...biubiubiubiubiubiubiubiubiu >>>>>>>>>>>>')
spinner.start()

rm(path.join(config.build.assetsRoot, config.build.assetsSubDirectory), err => {

	if(err) throw err;

	webpack(webpackConfig, function(err, stats) {
		
		spinner.stop()

		if(err) throw err

		process.stdout.write(stats.toString({
			
			colors: true,
			modules: false,
			children: false,
			chunks: false,
			chunkModules: false
			
		}) + '\n\n');

		if(stats.hasErrors()) {
			console.log(chalk.red('  打包出错了啊，出什么错我也不知道啊..........\n'))
			process.exit(1)
		}

		console.log(chalk.cyan('\n  打包成功!!!!!!.\n\n\n'))
		console.log(chalk.yellow(
			'  Tip: built files are meant to be served over an HTTP server.\n' +
			'  Opening index.html over file:// won\'t work.\n'
		))

	})
})