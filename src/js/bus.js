import Vue from 'vue'

import toast from "toast";


export default new Vue({

    data() {
        return {
            toast_instance:null,
            my_name:""
        }
    },
    methods: {

        toast_show(msg){

            if(this.toast_instance) {
                clearTimeout(this.toast_instance)
            }
            
            this.toast_instance= toast(msg);

        }
        
    }
})