// Vue 路由配置
import Vue from 'vue'
import VueRouter from 'vue-router'


import Bus from "bus";
Vue.use(VueRouter)

const routes = [

    // 注册
    {
        path: "/register",
        name: "register",
        component: () =>
            import ("view/register")
    },

    // 视频
    {
        path: "/video",
        name: "video",
        component: () =>
            import ("view/video")
    },

    // 音频
    {
        path: "/audio",
        name: "audio",
        component: () =>
            import ("view/audio")
    },
]

// 路由挂载
const router = new VueRouter({
    routes
})

router.beforeEach(function(to, from, next){

    if(!Bus.my_name && to.name!="register"){
        next("/register")
    }else{
        next()
    }

})

export default router