

import Vue from 'vue'
import App from './app.vue'
// import VueSocketio from 'vue-socket.io';
import VueSocketio from 'js/socket';

// 引入Vue配置
import router from 'js/router' // 路由

// 引入公共样式
import 'lib/fonts/iconfont'

// 引用Vue自定义插件

// 配置 socket url
// Vue.use(VueSocketio, `wss://${location.host}/call`);
Vue.use(VueSocketio, `/socket.io`);

// 创建vue实例，挂载路由
const appVue = new Vue({
    router,
    render: h => h(App)
}).$mount('#app-old')