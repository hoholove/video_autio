# video_autio


## 启动项目

> npm start

## 页面

位置：src/view/

 - 注册页面
 - 视频聊天 和 音频聊天放在一个页面，用一个变量控制切换

## 配合 websocket


给 Vue 添加 websocket监听事件，直接在组件里面的 选项 options 

```javascript

// 在这里监听事件，这里的事件 是给 后端触发的
sockets: {

    // 连接成功
    connected() {
        console.log("连接成功");
    },

    // 发送消息成功
    MsgSendOK(data) {
        alert(2222222);
        console.log("接收到消息---------->", data);
    }
},

```

触发 后端 的 websocket 事件


```javascript

// sendMsg 是后端的事件，在这里触发，并且发送数据
this.$socket.emit("sendMsg", 666666666888888888888888888);

```

## 打包

> npm run build

打包路径是 dist ，这里的页面资源是可以直接使用的


## 配置 websocket

在 src/main.js 中

> Vue.use(VueSocketio, `/socket.io`);

后面的路径，表示 后端的 websocket 路径