'use strict'

// 基础配置
const path = require('path')
const utils = require('./utils')
const config = require('../config')
const vueLoaderConfig = require('./vue-loader.conf')
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin')



function resolve(dir) {
	return path.join(__dirname, '..', dir)
}

module.exports = {
	entry: {
		app: './src/main'
	},
	output: {
		path: config.build.assetsRoot,
		filename: '[name].js',
		publicPath: process.env.NODE_ENV === 'production' ? config.build.assetsPublicPath : config.dev.assetsPublicPath
	},
	resolve: {
		extensions: ['.js', '.vue', '.json', ".png", ".jpg", ".styl", ".ts", ".css"],

		// fallback: [path.join(__dirname, '../node_modules')], // 解决路径找不到问题，配置成绝对路径
		// 给常访问文件夹起代号
		alias: {
			'vue$': 'vue/dist/vue.esm.js',
			'@': resolve('src'),
			'view': resolve('src/view'),
			'stylus': resolve('src/lib/stylus'),
			'lib': resolve('src/lib'),
			"static": resolve('static'),
			"js": resolve('src/js'),
			"bus": resolve('src/js/bus'),
			"toast": resolve('src/components/toast/index.js'),
		}
	},
	module: {
		rules: [{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: vueLoaderConfig
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				include: [resolve('src'), resolve('test')]
			},
			// 下面是 ts 配置-----------------
			{
				test: /\.ts$/,
				exclude: /node_modules/,
				enforce: 'pre',
				loader: 'tslint-loader'
			},
			{
				test: /\.tsx?$/,
				loader: 'ts-loader',
				exclude: /node_modules/,
				options: {
					appendTsSuffixTo: [/\.vue$/],
				}
			},
			// 上面是 ts 配置-------------------------
			{
				test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
				loader: 'url-loader',
				options: {
					limit: 10000,
					name: utils.assetsPath('img/[name].[hash:8].[ext]')
				}
			},
			{
				test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
				loader: 'url-loader',
				options: {
					limit: 10000,
					name: utils.assetsPath('media/[name].[hash:7].[ext]')
				}
			},
			{
				test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
				loader: 'url-loader',
				options: {
					limit: 10000,
					name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
				}
			}, {

				// 构建html过程中，解决html内部引用的img标签不会自动替换
				test: /\.html$/,
				loader: 'html-loader'
			},

		]
	},
	plugins: [

		new CopyWebpackPlugin([{
			from: resolve('static'),
			to: resolve('dist/static')
		}]),

	]
}